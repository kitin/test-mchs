$(function() {
	Pace.on("done", function(){
			$(".rafting-page").addClass('loaded');
	});
});

$(document).ready(function() {

    var boatSlider= $('.boat-slider');
    var newsSlider= $('.news-slider');

    boatSlider.slick({
        slidesToShow: 4.5,
        slidesToScroll: 1,
        arrows: false,
        infinite: false,
        responsive: [
          {
            breakpoint: 1200,
            settings: {
              slidesToShow: 3.5,
            }
          },
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 2.5,
            }
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 1.5,
            }
          },
        ]
    });

    newsSlider.slick({
        slidesToShow: 3.5,
        slidesToScroll: 1,
        arrows: false,
        infinite: false,
        responsive: [
          {
            breakpoint: 1200,
            settings: {
              slidesToShow: 2.5,
            }
          },
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 1.5,
            }
          },
        ]
    });

   $('.hero-parallax__layer').paroller();

});

